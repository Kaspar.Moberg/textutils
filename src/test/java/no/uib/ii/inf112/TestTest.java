package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	ITextAligner aligner = new ITextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			return " ".repeat(width - text.length()) + text;
		}

		public String flushLeft(String text, int width) {
			return text + " ".repeat(width - text.length());
		}

		public String justify(String text, int width) {
			String[] words = text.split("\\s+");

			int totalWidth = String.join("", words).length();
			int spaces = (width - totalWidth) / (words.length - 1);

			String returnString = "";
			for (int i = 0; i < words.length; i++){
				returnString += words[i];
				if (i >= words.length-1)
					break;
				returnString += " ".repeat(spaces);
			}

			return returnString;
		}};

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));

	}

	@Test
	void testRight(){
		assertEquals("    a", aligner.flushRight("a", 5));
		assertEquals("  arg", aligner.flushRight("arg", 5));
	}

	@Test
	void TestLeft(){
		assertEquals("a    ", aligner.flushLeft("a", 5));
		assertEquals("arg  ", aligner.flushLeft("arg", 5));
	}

	@Test
	void TestJustify(){
		assertEquals("fee   fie   foo", aligner.justify("fee fie foo", 15));
	}
}
